const express = require('express');
const bodyParser = require('body-parser');
const apiRoutes = require('./routes/apiRoutes');

const app = express();

const PORT = process.env.PORT || 8080;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api/files', apiRoutes);

app.listen(PORT, () => {
    console.log(`Server is running at PORT: ${PORT}`);
});
